FROM node:current-alpine

ENV AWS_CLI_VERSION=1.15.40

RUN apk --update --no-cache add \
    python \
    py-pip \
    jq \
    bash \
    git \
    curl \
    && pip install --no-cache-dir awscli==$AWS_CLI_VERSION \
    && apk del py-pip \
    && rm -rf /var/cache/apk/* /root/.cache/pip/* /usr/lib/python2.7/site-packages/awscli/examples \
    && npm i -g rollup


